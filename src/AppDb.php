<?php

namespace HyveMobileTest;

require '../boot.php';

/**
 * @author Gershon KOks <gershon.koks@yahoo.co.za>
 * 
 * Application Database class used
 * to establish connection to DB and
 * execute queries.
 */
class AppDb {

    /**
     * The Database Object
     *
     * @var MysqliDb
     */
    protected $DB;

    /**
     * Is connected to database
     *
     * @var bool
     */
    public $isConnected;

    /**
     * Class Constructor
     */
    public function __construct() {
        $this->init();
    }

    /**
     * Instantiation tasks before startup.
     *
     */
    protected function init() {
        try {
            $this->DB = new \MysqliDb($this->getDbConfig());
            $this->DB->connect();
            $this->isConnected = true;
            $this->DB->rawQuery('CREATE DATABASE IF NOT EXISTS hyve');
            $this->DB->rawQuery('CREATE TABLE IF NOT EXISTS hyve.contacts (
                id INT,
                title VARCHAR(50),
                first_name VARCHAR(255),
                last_name VARCHAR(255),
                email VARCHAR(100),
                ip_address VARCHAR(15),
                timezone VARCHAR(6),
                contact_card TEXT,
                dt DATE,
                tm TIME,
                note TEXT CHARACTER SET utf8mb4
            )');
            $this->DB->disconnect();
        } catch(\Exception $ex) {
            $this->isConnected = false;
            throw $ex;
        }
    }
    
    /**
     * Insert new Record into DB table
     *
     * @param string $table
     * @param array $data
     * @return array
     */
    public function add(string $table, array $data) : array {
        $record = [];
        $this->DB->connect();
        $id = $this->DB->insert($table, $data);
        $this->DB->where('id', $id);
        $record = $this->DB->getOne($table);
        if (!$id) throw new \Exception($this->DB->getLastError());
        $this->DB->disconnect();
        return $record;
    }

    /**
     * Insert Mutiple Records
     * into database table
     *
     * @param string $table
     * @param array $data
     * @return array
     */
    public function addMulti(string $table, array $data) {
        $this->DB->connect();
        $ids = $this->DB->insertMulti($table, $data);
        $this->DB->where('id', $ids, 'IN');
        $records = $this->DB->get($table);
        if (!$ids) throw new \Exception($this->DB->getLastError());
        $this->DB->disconnect();
    }

    /**
     * Get database table record
     * by were condition
     *
     * @param string $table
     * @param string $field
     * @param string $value
     * @return void
     */
    public function getWhere(string $table, string $field, string $value) {
        $records = [];
        $this->DB->connect();
        $ids = $this->DB->where($field, $value);
        $records = $this->DB->get($table);
        if (!$ids) throw new \Exception($this->DB->getLastError());
        $this->DB->disconnect();
        return $records;
    }

    /**
     * Get All table records from
     * datanase table
     *
     * @param string $table
     * @return void
     */
    public function getAll(string $table) {
        $records = [];
        $this->DB->connect();
        $records = $this->DB->get($table);
        $this->DB->disconnect();
        return $records;
    }

    /**
     * Execute raw database query.
     *
     * @param string $query
     * @return array
     */
    public function raw(string $query) : array {
        $this->DB->connect();
        $results = $this->DB->rawQuery($query);
        if (!empty($this->DB->getLastError())) throw new \Exception($this->DB->getLastError());
        $this->DB->disconnect();
        return $results;
    }

    /**
     * Derive database connection
     * config from file
     *
     * @throws Exception
     * @return array
     */
    protected function getDbConfig($configFile = "../.conf") : array {
        $config = [];
        try {
            $file = fopen($configFile, "r");
            while(!feof($file))
            {
                $split = explode('=', trim(fgets($file)));
                if(!$split[0]) continue;
                $config[$split[0]] = $split[1];
            }
            fclose($file);
        } catch (\Exception $ex) {
            throw $ex;
        }
        return $config;
    }

}