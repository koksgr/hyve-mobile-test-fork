<?php

namespace HyveMobileTest;

require '../boot.php';

/**
 * File Types that can be read.
 */
class FILETYPE {
    const ZIP   = 'zip';
    const GZ    = 'gz';
    const FLAT  = 'flat';
}

/**
 * @author Gershon Koks <gershon.koks@yahoo.co.za>
 * 
 * This class reads a single source
 * of a specific file type. 
 */
class FileHandler {

    /**
     * Source File
     *
     * @var string
     */
    protected $filename;

    /**
     * File Name of Source stored
     * within compressed strinf
     *
     * @var string
     */
    protected $streamFileName;

    /**
     * Input Type
     *
     * @var FILETYPE
     */
    protected $filetype;

    /**
     * Allow Header or Not
     *
     * @var FILEHEADER
     */
    protected $allowFileHeader;

    /**
     * Class Constructor
     *
     * @param string $newFile
     */
    public function __construct(string $newFile, string $type = null, string $strmFileName = null, bool $allowHeader = true) {
        $this->filename = $newFile;
        if (!is_null($type)) $this->filetype = $type;
        if (!is_null($strmFileName)) $this->streamFileName = $strmFileName;
        if (!is_null($allowHeader)) $this->allowFileHeader = $allowHeader;
    }
    
    /**
     * Set the Source File Name
     *
     * @param string $newFile
     */
    public function setFileName(string $newFile) {
        $this->filename = $newFile;
    }

    /**
     * Get the Source File Name
     *
     * @return string
     */
    public function getFileName() : string {
        return $this->filename;
    }

    /**
     * Get File Type
     */
    public function ofType() {
        return $this->filetype;
    }

    /**
     * Set the File Type
     *
     * @param integer $type
     */
    public function setType(int $type) {
        $this->filetype = $type;
    }

    /**
     * Get Stream File Name
     *
     * @return string
     */
    public function streamFileName() {
        return $this->streamFileName;
    }

    /**
     * Set Stream File Name
     *
     * @param string $strFileName
     */
    public function setStreamFileName(string $strFileName) {
        $this->streamFileName = $strFileName;
    }

    /**
     * Read all data from file
     * as array of records
     *
     * @throws FileNotFoundException
     * @return array
     */
    public function read() : array {
        $this->hasFile();
        switch ($this->filetype) {
            case FILETYPE::ZIP:
                $data = $this->asZip();
                break;
            default:
                $data = [];
                break;
        }
        return $data;
    }

    /**
     * Read Source File as ZIP stream
     *
     * @throws StreamFileNotFoundException
     * @return array
     */
    protected function asZip() : array {
        $contents   = '';
        $data       = [];
        $idx        = 0;
        $z          = new \ZipArchive();
        $z->open($this->filename);
        $fp = $z->getStream($this->streamFileName);
        if(!$fp) throw new StreamFileNotFoundException();
        while (!feof($fp)) {
            array_push($data,trim(fgets($fp)));
        }
        fclose($fp);
        if (!$this->allowFileHeader) array_shift($data);
        return $data;
    }

    /**
     * Validate if the input file source exists.
     */
    protected function hasFile() {
        if (is_dir($this->filename)) throw new FileNotFoundException("Given Source File Name is a directory.");
        if(!file_exists($this->filename)) throw new FileNotFoundException("Source File does not exsts.");
    }
}