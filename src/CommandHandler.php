<?php

namespace HyveMobileTest;

require '../boot.php';

/**
 * @author Gershon koks <gershon.koks@yahoo.co.za>
 * 
 * A Simple Class that handles
 * basic commands when executing
 * the main imorting jobs.
 */
class CommandHandler {

    protected $commands;

    public function __construct($args) {
        foreach($args as $idx => $arg) {
            if ($idx === 0) continue;
            $split = explode('=', $arg);
            if ($split) {
                $this->commands[$split[0]] = $split[1];
            }
        }
        $this->validateCommands();
    }

    /**
     * Excute to Import Job
     *
     * @return void
     */
    public function execute() {
        // should allow fileeader record?
        $time_pre = microtime(true);
        print_r("Initiating Work \n");
        $allowFileHeader = (empty($this->commands['header'])) ? false : true;
        // init import file class  
        $fi = new \HyveMobileTest\ImportFromFile($this->commands['if'], $this->commands['filetype'], $this->commands['stream'], $allowFileHeader);
        print_r("Reading raw file data  \n");
        $fi->init();
        // set the file records structure
        $fi->setFileRecStruct();
        // enable/disable deduping
        if (!empty($this->commands['dedup'])) {
            $dedup = (bool)$this->commands['dedup'];
            print_r("File Deduping  \n");
            $fi->fileDedup(true);
        }
        // process partition block or not
        if (!empty($this->commands['partitions']) && !empty($this->commands['block'])) {
            print_r("Partition File Data  \n");
            $partitions = (int) $this->commands['partitions'];
            $block = (int) $this->commands['block'];
            $fi->partitionProcessBlock($partitions, $block);
        }
        // map the data
        print_r("Mapping Data  \n");
        $fi->map();
        // import the mapped data to db
        print_r("Importing Data  \n");
        $fi->import();
        print_r("Finish!!! \n");
        $time_post = microtime(true);
        $exec_time = $time_post - $time_pre;
        print_r("Job took $exec_time to complete");
    }

    /**
     * Run command validations against
     * specific commands.
     */
    protected function validateCommands() {
        if (!isset($this->commands['if']) || empty($this->commands['if'])) {
            echo "ERROR: No input file specified. \n  "; die;
        } else if(!isset($this->commands['header']) || empty($this->commands['header'])) {
            echo "ERROR: Need to specify if file header record should be allowed or not. \n  "; die;
        } else if (!isset($this->commands['filetype']) || empty($this->commands['filetype'])) {
            echo "ERROR: No File Type provided. \n  "; die;
        } else if (!isset($this->commands['stream']) || empty($this->commands['stream'])) {
            echo "ERROR: No File Stream File provided. \n  "; die;
        }
    }

}