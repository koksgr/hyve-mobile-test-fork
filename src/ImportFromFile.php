<?php

namespace HyveMobileTest;

require '../boot.php';

/**
 * @author Gershon Koks <gershon.koks@yahoo.co.za>
 * 
 * This Class provides a structure
 * to the raw file data before impoting
 * the data into a DB table.
 */
class ImportFromFile extends FileHandler {

    use HyveDataTransforms;

    /**
     * FIle Record Structure
     *
     * @var array
     */
    protected $fileDataStructure;

    /**
     * Total Raw File Records
     *
     * @var int
     */
    protected $totalRawRecords;

    /**
     * Unstructured raw file data
     *
     * @var array
     */
    public $rawFileData;

    /**
     * Array of mapped file data
     *
     * @var array
     */
    public $mappedFileData;
    
    /**
     * Array of fully mapped records
     * required for db table insert.
     *
     * @var array
     */
    public $mappedRecords;

    /**
     * Task to execute before processing.
     *
     * @return void
     */
    public function init() {
        if ($this->allowFileHeader) {
            $idx = 0;
            $fileData = $this->read();
            for($i = 0; $i < count($fileData); $i++) {
                if ($i === 0) {
                    $this->fileDataStructure = explode(',', $fileData[0]);
                    continue;
                }
                $this->rawFileData[$idx] = $fileData[$i];
                ++$idx;
            }
        } else {
            $this->rawFileData = $this->read();
        }
        $this->totalRawRecords  = count($this->rawFileData);
    }

    /**
     * Set the srouce file data structure
     *
     * @param array $struct
     * @return void
     */
    public function setFileRecStruct(array $struct = null) {
        if(!is_null($struct)) $this->fileDataStructure = $struct;
    }

    /**
     * Get the source file data structure
     *
     * @return array
     */
    public function getFileRecStruct() : array {
        return $this->fileDataStructure;
    }

    /**
     * Get Total file records
     *
     * @return void
     */
    public function getTotalRawRecords() {
        return $this->totalRawRecords;
    }

    /**
     * Dedup raw file data
     *
     * @param boolean $dedup
     * @return void
     */
    public function fileDedup($dedup = false) {
        if ($dedup) {
            $hashHolder     = [];
            $dedupedData    = [];
            foreach ($this->rawFileData as $rec) {
                $hash = md5($rec);
                if (!in_array($hash, $hashHolder)) {
                    array_push($hashHolder, $hash);
                    array_push($dedupedData, $rec);
                }
            }
            $this->rawFileData = $dedupedData;
        }
    }

    /**
     * Scale Down raw file data into single
     * partition block to process.
     *
     * @param integer $into
     * @param integer $processBlock
     */
    public function partitionProcessBlock(int $into, int $processBlock) {
        if ($processBlock > $into) throw new \Exception("Process block cannot be bigger than specified partition level.");
        $tatalRawRec    = $this->getTotalRawRecords();
        $remainder      = $tatalRawRec % $into;
        if ($remainder) {
            if ($into === $processBlock) {
                $totalToProcess = (int) ($tatalRawRec / $into) + $remainder;
                $startPos       = (int) ($tatalRawRec / $into) * ($processBlock - 1) + 1;
                $endPos         = (int) ($startPos + $totalToProcess) - 2;
            } else {
                $totalToProcess = (int)($tatalRawRec / $into);
                $startPos       = (int) ($totalToProcess * ($processBlock - 1));
                $endPos         = (int) ($startPos + $totalToProcess) - 1;
            }
        } else {
            $totalToProcess = (int)($tatalRawRec / $into);
            $startPos       = (int) ($totalToProcess * ($processBlock - 1));
            $endPos         = (int) ($startPos + $totalToProcess) - 1;
        }
        $partition      = [];
        $idx            = 0;
        for($pos = $startPos; $pos <= $endPos; $pos++) {
            $partition[$idx] = $this->rawFileData[$pos];
            ++$idx;
        }
        $this->rawFileData = $partition;
    }

    /**
     * Run all mapping tasks
     *
     * @return void
     */
    public function map() {
        $this->mappedFileData = $this->mapFileData();
        $this->mapHyveRecords();
    }

    /**
     * Execute database import.
     *
     * @return boolean
     */
    public function import() : bool {
        $didImport = false;
        try {
            $db = new \HyveMobileTest\AppDb();
            $data = $db->addMulti('hyve.contacts', $this->mappedRecords);
            $didImport = true;
        } catch(\Exception $e) {
            throw $e;
        }
        
        return $didImport;
    }

    /**
     * Hyve specific file data map to
     * main record structure required db
     * datanase table.
     *
     * @return void
     */
    protected function mapHyveRecords() {
        $this->mappedRecords = [];
        foreach ($this->mappedFileData as $fileRec) {
            $tmp                = [];
            $tmp['id']          = $fileRec['id'];
            $tmp['title']       = $fileRec['title'];
            $tmp['first_name']  = $fileRec['first_name'];
            $tmp['last_name']   = $fileRec['last_name'];
            $tmp['email']       = $fileRec['email'];
            $tmp['ip_address']      = $this->ipLookup($fileRec['email']);
            $tmp['contact_card']    = $this->generateJPG("../resources/img/", $fileRec['first_name'],$fileRec['last_name'],$fileRec['email'], true);
            $tmp['timezone']    = $this->setDateTimeTimezone($fileRec['tz'], $fileRec['date'], $fileRec['time']);
            $tmp['dt']          = date('Y-m-d');
            $tmp['tm']          = date('H:i:s');
            $tmp['note']        = $fileRec['note'];
            array_push($this->mappedRecords, $tmp);
        }
    }

    /**
     * Map raw file data to
     * raw file data strucuture.
     *
     * @return array
     */
    protected function mapFileData() : array {
        $data = [];
        for ($i=0; $i < count($this->rawFileData); $i++) {
            $rec = explode(',', $this->rawFileData[$i]);
            for ($f=0; $f < count($this->fileDataStructure); $f++) {
                $tmp[$this->fileDataStructure[$f]] = $rec[$f];
            }
            array_push($data, $tmp);
        }
        return $data;
    }
}