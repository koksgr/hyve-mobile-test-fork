<?php

namespace HyveMobileTest;

require '../boot.php';

use NicoVerbruggen\ImageGenerator\ImageGenerator;

/**
 * @author Gershon Koks <gershon.koks@yahoo.co.za>
 * 
 * Hyve import file specific
 * data transformation functions.
 */
trait HyveDataTransforms {

   
    
    /**
     * Set DateTime TimeZone
     *
     * @param string $timezone
     * @param string $date
     * @param string $time
     * @return string
     */
    protected function setDateTimeTimezone(string $timezone, string $date, string $time) : string {
        $datetime = new \DateTime("$date $time", new \DateTimeZone($timezone));
        return $datetime->format('P');
    }

    /**
     * Date formatter
     *
     * @param string $dt
     * @param string $format
     * @return string
     */
    protected function dateFormatter(string $dt, string $format) : string {
        $datetime = new \DateTime($dt);
        $formatter = $datetime->format($format);
        return $formatter;
    }

    //TODO: php gethostbyname function to slow
    // need to find another way to resolve this
    // Perhaps whois api lookup.
    protected function ipLookup($email) : string {
        $domain = explode('@', $email)[1];
        return (checkdnsrr($domain , "A")) ? gethostbyname($domain) : "0.0.0.0";
    }

    /**
     * Generate JPG file
     *
     * @param string $path storate path to store image
     * @param string $firstname
     * @param string $lastname
     * @param string $email
     * @param boolean $deleteImages delete image or not
     * @return string
     */
    protected function generateJPG(string $path, string $firstname, string $lastname, string $email, bool $deleteImages = false) : string {
        $content    = "$firstname $lastname $email";
        $image      = $path.$firstname."_".$lastname.".jpg";
        $generator  = new ImageGenerator([
            'targetSize' => "500x240",
        ]);
        $generator->makePlaceholderImage(
            $content,
            $image
        );
        return $image;
    }

}