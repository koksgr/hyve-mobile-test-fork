<?php

namespace HyveMobileTest;

class FileNotFoundException extends \Exception
{
    public function __construct(string $message = null, int $code = 0, Exception $previous = null)
    {
        if (null === $message) {
            $message = 'File could not be found.';
        }
        parent::__construct($message, $code, $previous);
    }
}