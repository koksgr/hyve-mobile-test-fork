<?php

namespace HyveMobileTest;

require '../boot.php';

/**
 * @author Gershon KOks <hershon.koks@yahoo.co.za>
 * 
 * Simple API Class that controls
 * requests based on _GET request params.
 */
class AppApi {

    /**
     * Index View
     *
     */
    public static function index() {
        try {
            $db = new AppDb();
            $data = $db->getAll('hyve.contacts');
        } catch(\Exception $e) {
            return $e->getMessage();
        }
        echo json_encode($data, JSON_FORCE_OBJECT|JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    }

    /**
     * Contacts View
     *
     */
    public static function contacts() {
        $db = new AppDb();
        try {
            $data = $db->getAll('hyve.contacts');
        } catch(\Exception $e) {
            print_r($e->getMessage());
        }
        $numrows = count($data);
        if (isset($_GET['rowsperpage']) && is_numeric($_GET['rowsperpage'])) {
            $rowsperpage = (int) $_GET['rowsperpage'];
        } else {
            $rowsperpage = 10;
        }
        
        $totalpages = ceil($numrows / $rowsperpage);
        // get the current page or set a default
        if (isset($_GET['page']) && is_numeric($_GET['page'])) {
            $currentpage = (int) $_GET['page'];
        } else {
            $currentpage = 1;  // default page number
        }
        // if current page is greater than total pages
        if ($currentpage > $totalpages) $currentpage = $totalpages;
        // if current page is less than first page
        if ($currentpage < 1) $currentpage = 1;
        // the offset of the list, based on current page
        $offset = ($currentpage - 1) * $rowsperpage;
        try {
            $rows = $db->raw("SELECT * FROM hyve.contacts ORDER BY id ASC LIMIT $offset, $rowsperpage");
        } catch(\Exception $e) {
            print_r($e->getMessage());
        }
        
        echo json_encode($rows, JSON_FORCE_OBJECT|JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    }

    /**
     * Timezone View
     */
    public static function timezones() {
        $tz = (isset($_GET['timezone'])) ? str_replace("%3A", ":", urlencode($_GET['timezone'])) : null;
        $db = new AppDb();
        $response = [];
        try {
            if (!isset($tz)) {
                $dbData = $db->raw("select count(*) as total, timezone from hyve.contacts group by timezone");
                foreach ($dbData as $rec) {
                    $tz = $rec['timezone'];
                    $data = [
                        'timezone'  => $tz,
                        'total'     => $rec['total'],
                    ];
                    $data['contacts'] = $db->raw("SELECT * FROM hyve.contacts WHERE timezone = '$tz'");
                    array_push($response, $data);
                }
            } else {
                $dbData = $db->raw("SELECT count(*) AS total, timezone FROM hyve.contacts WHERE timezone = '$tz' GROUP BY timezone");
                foreach ($dbData as $rec) {
                    $tz = $rec['timezone'];
                    $response = [
                        'timezone'  => $rec['timezone'],
                        'total'     => $rec['total'],
                    ];
                    $response['contacts'] = $db->raw("SELECT * FROM hyve.contacts WHERE timezone = '$tz'");
                }
            }
        } catch(\Exception $e) {
            print_r($e->getMessage());
        }
        echo json_encode($response, JSON_FORCE_OBJECT|JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    }

}