<?php

class AppDatabaseTest extends \PHPUnit\Framework\TestCase {

    protected $appDB;

    protected function setUp() {
        $this->appDB = new \HyveMobileTest\AppDb();
    }

    /** @test */
    public function set_dabase_config() {
        $this->assertEquals(true, $this->appDB->isConnected);
    }

    /** @test */
    public function run_raw_query() {
        $results = $this->appDB->raw('truncate hyve.contacts');
        $this->assertInternalType('array', $results);
    }

    /** @test */
    public function table_insert() {
        $contact = [
            'id'            => '1',
            'title'         => 'Mr',
            'first_name'    => 'Gershon',
            'last_name'     => 'Koks',
            'email'         => 'gershon.koks@yahoo.co.za',
            'ip_address'    => '127.0.0.1',
            'contact_card'  => 'blahblahblahblahblahblahblahblahblahblah',
            'timezone'      => '+02:00',
            'dt'            => '2000-01-01',
            'tm'            => '11:12:13',
            'note'          => 'tester'
        ];
        $newContact = $this->appDB->add('hyve.contacts', $contact);
        $this->assertEquals("Gershon", $newContact['first_name']);
    }

    /** @test */
    public function table_multi_insert() {
        $contact = [
            [
                'id'            => '2',
                'title'         => 'Mr',
                'first_name'    => 'John',
                'last_name'     => 'Doe',
                'email'         => 'john.doe@yahoo.co.za',
                'ip_address'    => '127.1.1.1',
                'contact_card'  => 'blahblahblahblahblahblahblahblahblahblah',
                'timezone'      => '+02:00',
                'dt'            => '2000-02-02',
                'tm'            => '11:12:13',
                'note'          => '0️⃣ 1️⃣ 2️⃣ 3️⃣ 4️⃣ 5️⃣ 6️⃣ 7️⃣ 8️⃣ 9️⃣ 🔟'
            ],
            [
                'id'            => '3',
                'title'         => 'Mrs',
                'first_name'    => 'Courtney',
                'last_name'     => 'Koks',
                'email'         => 'courtneykoks@yahoo.co.za',
                'ip_address'    => '127.0.29.2',
                'contact_card'  => 'blahblahblahblahblahblahblahblahblahblah',
                'timezone'      => '+02:00',
                'dt'            => '2000-01-02',
                'tm'            => '11:12:13',
                'note'          => 'DROP TABLE users'
            ]
        ];
        $newContacts = $this->appDB->addMulti('hyve.contacts', $contact);
        $this->assertContains("John", $this->appDB->getWhere('hyve.contacts', 'id', '2')[0]);
    }

    /** @test */
    public function get_all_by_condition() {
        $appDb      = new \HyveMobileTest\AppDb();
        $contacts   = $appDb->getWhere('hyve.contacts', 'tm', '11:12:13');
        $this->assertCount(3, $contacts); 
    }

}