<?php

class FileReadDataTest extends \PHPUnit\Framework\TestCase
{

    private $filename;
    private $strmFileName;
    private $fileHandler;

    protected function setUp() {
        $this->filename     = "../resources/data.csv.zip";
        $this->strmFileName = "MOCK_DATA.csv";
        $this->fileHandler  = new \HyveMobileTest\FileHandler($this->filename);
    }

    /** @test */
    public function instantiate_with_file_name() {
        $this->assertEquals($this->filename, $this->fileHandler->getFileName());
    }

    /** @test */
    public function set_new_file_name() {
        $this->fileHandler->setFileName("testing.dat");
        $this->assertEquals("testing.dat", $this->fileHandler->getFileName());
    }

    /** @test */
    public function is_dir_exception() {
        $this->expectException(\HyveMobileTest\FileNotFoundException::class);
        $fh = new \HyveMobileTest\FileHandler("../resources/");
        $fh->read();
    }

    /** @test */
    public function file_not_found_exception() {
        $this->expectException(\HyveMobileTest\FileNotFoundException::class);
        $fh = new \HyveMobileTest\FileHandler("../resources/xzy.dat");
        $fh->read();
    }

    /** @test */
    public function set_zip_file_type() {
        $fh = new \HyveMobileTest\FileHandler($this->filename, \HyveMobileTest\FILETYPE::ZIP);
        $this->assertEquals(\HyveMobileTest\FILETYPE::ZIP, $fh->ofType());
    }

    /** @test */
    public function init_with_stream_file_name() {
        $fh = new \HyveMobileTest\FileHandler($this->filename, \HyveMobileTest\FILETYPE::ZIP, $this->strmFileName);
        $this->assertEquals($this->strmFileName, $fh->streamFileName());
    }

    /** @test */
    public function set_stream_file_name() {
        $this->fileHandler->setStreamFileName($this->strmFileName);
        $this->assertEquals($this->strmFileName, $this->fileHandler->streamFileName());
    }

    /** @test */
    public function stream_file_not_found_exception() {
        $fh = new \HyveMobileTest\FileHandler($this->filename, \HyveMobileTest\FILETYPE::ZIP, "tester.csv");
        $this->expectException(\HyveMobileTest\StreamFileNotFoundException::class);
        $fh->read();
    }

    /** @test */
    public function read_stream_data_as_array() {
        $fh = new \HyveMobileTest\FileHandler($this->filename, \HyveMobileTest\FILETYPE::ZIP, $this->strmFileName);
        $content = $fh->read();
        $this->assertContains('id,title,first_name,last_name,email,tz,date,time,note', $content);
    }

    /** @test */
    public function include_file_header_record() {
        $fh = new \HyveMobileTest\FileHandler($this->filename, \HyveMobileTest\FILETYPE::ZIP, $this->strmFileName, true);
        $content = $fh->read();
        $this->assertEquals('id,title,first_name,last_name,email,tz,date,time,note', $content[0]);
    }

    /** @test */
    public function exclude_file_header_record() {
        $fh = new \HyveMobileTest\FileHandler($this->filename, \HyveMobileTest\FILETYPE::ZIP, $this->strmFileName, false);
        $content = $fh->read();
        $this->assertEquals('1,Ms,Darnall,Hamp,dhamp0@moonfruit.com,Europe/Paris,30-Apr-2017,20:32:47,‪‪test‪', $content[0]);
    }

}
