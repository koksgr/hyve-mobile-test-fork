<?php

class ImportFromFileTest extends \PHPUnit\Framework\TestCase {

    public $fi;

    protected function setUp() {
        $this->fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
    }

    /** @test */
    public function define_target_data_structure_from_file_header() {
        $this->fi->init();
        $this->fi->setFileRecStruct();
        $this->assertEquals(
            ['id','title','first_name','last_name','email','tz','date','time','note'],
            $this->fi->getFileRecStruct()
        );
    }

    /** @test */
    public function define_custom_target_data_structure() {
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", false);
        $fi->init();
        $struct = ['title', 'first_name', 'last_name', 'email', 'ip_addres', 'tz', 'contact_card', 'timezone', 'dt', 'tm', 'note'];
        $fi->setFileRecStruct($struct);
        $this->assertEquals($struct, $fi->getFileRecStruct());
    }

    /** @test */
    public function file_record_dedup() {
        $this->fi->setFileRecStruct();
        $this->fi->init();
        $this->fi->rawFileData = ["xxxx", "xxxx"];
        $countBefore = count($this->fi->rawFileData);
        $this->fi->fileDedup(true);
        $countAfter = count($this->fi->rawFileData);
        $this->assertEquals(1, $countAfter);
    }
    
    /** @test */
    public function map_file_data_to_record_structure() {
        $appDb = new \HyveMobileTest\AppDb();
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $fi->fileDedup(true);
        $fi->map();
        $this->assertEquals("dhamp0@moonfruit.com", $fi->mappedFileData[0]['email']);
    }

    /** @test */
    public function map_hyve_data_record() {
        $appDb = new \HyveMobileTest\AppDb();
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $fi->fileDedup(true);
        $fi->map();
        $this->assertNotEquals("0.0.0.0", $fi->mappedRecords[0]['ip_address']);
    }

    /** @test */
    public function get_timezone_from_date_and_time_fields() {
        $appDb = new \HyveMobileTest\AppDb();
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $fi->fileDedup(true);
        $fi->map();
        $this->assertNotEquals("+11:00", $fi->mappedRecords[0]['timezone']);
    }

    /** @test */
    public function generate_contact_card() {
        $appDb = new \HyveMobileTest\AppDb();
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $fi->fileDedup(true);
        $fi->map();
        $this->assertNotEquals("blahblah", $fi->mappedRecords[0]['contact_card']);
    }

    /** @test */
    public function import_to_db() {
        $appDb = new \HyveMobileTest\AppDb();
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $fi->fileDedup(true);
        $fi->map();
        $didImport = $fi->import();
        $this->assertEquals(true, $didImport);
    }

    /** @test */
    public function get_tatal_file_records_no_header() {
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", false);
        $struct = ['title', 'first_name', 'last_name', 'email', 'ip_addres', 'tz', 'contact_card', 'timezone', 'dt', 'tm', 'note'];
        $fi->init();
        $fi->setFileRecStruct($struct);
        $total = $fi->getTotalRawRecords();
        $this->assertNotEquals(0, $total);
        $this->assertEquals(1000, $total);
    }

    /** @test */
    public function get_tatal_file_records_header() {
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $total = $fi->getTotalRawRecords();
        $this->assertNotEquals(0, $total);
        $this->assertEquals(1000, $total);
    }

    /** @test */
    public function process_partitioned_data() {
        $fi = new \HyveMobileTest\ImportFromFile("../resources/data.csv.zip", \HyveMobileTest\FILETYPE::ZIP, "MOCK_DATA.csv", true);
        $fi->init();
        $fi->setFileRecStruct();
        $fi->fileDedup(true);
        $fi->partitionProcessBlock(4, 2);
        $fi->map();
        $this->assertEquals(250, count($fi->mappedRecords));
    }
}