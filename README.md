#Theory Answers

##Answer 1
Automoted testing is best used within a CI(Continuous Integration) environment thus
allowing for rapid development against mostly MVP driven projects. In addition it allows the following:

- Find bugs early in development phase
- reduces the need for manual testing
- more flexible development processes

##Answer 2
####php Threading (pthread)
By default,PHP does not include threading functionality, thus the need to install
a separate library namely "pthread". 
#####pros
- Processing tasks can be separated in an async or Parallel manner thus improving
performance over labour intensive computing tasks.
#####cons
- Large scale thread management can be hard at times to debug
- Depending on DevOps experience, setting up pthread can be hard.
- At times outputs unexpected results
####Queue Messaging .i.e RabitMQ
#####pros
- Works best in an multi server environment.
- Delegates intensive computing jobs to multiple servers.
- Great for state management
- Programming languages that does not support threading can use
Message Queue to execute serial threaded tasks in parallel.
#####cons
- Some messaging servers might not conform to FIFO
- might require dequeuing before performing specific task which can
result in unwanted output.
####Caching file level or in-memory
#####pros
- Has all the pros as Queue Messaging
- In-memory caching servers has String key hashing which helps with performance
and reduces the risks of experiencing unexpected output task results.
#####cons
- Can have limitation within a multi server environment.
- On file level caching state management or various task can be hard to manage
- Might not work very well with larger Queue Messaging

#Dependencies
- php >= 7.0
- php-zip
- composer install
- php-gd

#Config
Within the project root directory, create a ".conf" file. Within the file provide the following:

- host=127.0.0.1
- port=3306
- database=db_name
- username=db_user
- password=db_user_password
- charset=utf8mb4

Note: Only the above list of settings are required, any additional settings will be ignored.

#Usage
If you are making use of a database named 'hyve', then please note that a table named 'contacts' will be added to the schema.

execute the following command from the src/ location:
####php run_data_import.php if=../resources/data.csv.zip  stream=MOCK_DATA.csv header=true filetype=zip dedup=true

####php command options

- **[if]** the input source file 
- **[stream]** the stream file stored within zip
- **[header]** include or exclude source file header record
- **[filtype]** type of source file to process [zip, gz, flat]
- **[dedup]** remove duplicates from file

####Block batch process input file
The following commands can be run from mutiple servers on the same input file thus allowing a specific partiioned block to be process and load the data to the same target database table. The examples commands below will process a single source file in four partions. (The commands can be executed in any order)

####php run_data_import.php if=../resources/data.csv.zip  stream=MOCK_DATA.csv header=true filetype=zip dedup=true partitions=4 block=1

####php run_data_import.php if=../resources/data.csv.zip  stream=MOCK_DATA.csv header=true filetype=zip dedup=true partitions=4 block=2

####php run_data_import.php if=../resources/data.csv.zip  stream=MOCK_DATA.csv header=true filetype=zip dedup=true partitions=4 block=3

####php run_data_import.php if=../resources/data.csv.zip  stream=MOCK_DATA.csv header=true filetype=zip dedup=true partitions=4 block=4


Access the following Routes
####GET/public/index.php (lists all data from file)
####GET/public/contacts.php (default 10 contacts)
####GET/public/contacts.php?page=1&rowsperpage=2 (two rows of data on page 2)
####GET/public/contacts.php?timezone=Africa/Johannesburg (Lists Contacts of timezone)
####GET/public/timezones.php (Contacts per timezone)
####GET/public/timezones.php?timezone=Africa/Johannesburg (Contacts in TimeZone)


#Release Features
- Pre Create DB ojects
- Unzip compressed file
- Maps DateTime Timezone
- Generates Contact Card JPG image
- basic cli
- batch processing